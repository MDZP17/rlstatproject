from sqlalchemy import Boolean, Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship
from .database import Base

class Equipe(Base):
    __tablename__ = "equipes"
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, index=True, unique=True)
    matchs = relationship("Match", back_populates="equipe")
    #players = relationship("Player", back_populates="")

class Match(Base):
    __tablename__ = "matchs"
    id = Column(Integer, primary_key=True, index=True)
    score = Column(Integer, index=True)
    id_equipe = Column(Integer, ForeignKey("equipes.id"))
    equipe = relationship("Equipe", back_populates="matchs")

class Player(Base):
    __tablename__ = "players"
    id_player = Column(Integer, primary_key=True, index=True)
    name_player = Column(String, index=True)
    id_equipe = Column(Integer, ForeignKey("equipes.id"))

