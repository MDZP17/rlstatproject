from scrapy import Selector
import pandas as pd
import glob

Lienhtml = glob.glob("events/*.html")

df = pd.DataFrame(columns=['id', 'Nom de la competition', 'Date', 'Type du match', 'Team1', 'Team2', 'ScoreTeam1', 'ScoreTeam2'])

for lien in Lienhtml:
    f = open(lien, 'r')
    message = f.read()

    if message:
        selector = Selector(text=message)
        title = selector.css("div.event-tag::text").get()
        matchs = selector.css("tr").getall()
        matchs = matchs[1:]
        for match in matchs:
            info = Selector(text=match)
            test = info.css("td.vuetable-slot").getall()

            info1 = Selector(text=test[0])
            date = info1.css("td::text").get().strip()

            info2 = Selector(text=test[1])
            type = info2.css("td::text").get().strip()

            info3 = Selector(text=test[2])
            team1 = info3.css("div::text").get()

            info4 = Selector(text=test[4])
            team2 = info4.css("div::text").get()

            info5 = Selector(text=test[3])
            score = info5.css("span::text").getall()
            ScoreTeam1, ScoreTeam2 = int(score[0]), int(score[1])
            id = len(df)
            L = [id, title, date, type, team1, team2, ScoreTeam1, ScoreTeam2]
            df.loc[id] = L
    print(df)
file = df.to_csv('myfile2.csv', header = True, index = False, sep = ';')